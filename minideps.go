package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"bitbucket.org/ulfurinn/cli"
)

const (
	repoHg  = "hg"
	repoGit = "git"
	repoBrz = "bzr"
)

func main() {
	app := cli.NewApp()
	app.Main.Commands = []cli.Command{
		{
			Name:   "collect",
			Action: collect,
			Options: []cli.Option{
				cli.StringSliceOption{
					Name:  "X",
					Usage: "ignore packages under this path; can be specified multiple times",
				},
			},
		},
		{
			Name:   "restore",
			Action: restore,
		},
		{
			Name:   "update",
			Action: update,
		},
	}
	if err := app.Run(os.Args[1:]); err != nil {
		fmt.Fprintln(os.Stderr, err)
		if err, ok := err.(cli.Exit); ok {
			os.Exit(err.StatusCode)
		} else {
			os.Exit(1)
		}
	}
}

type dep struct {
	Path     string
	RepoType string
	RepoPath string
	ID       string
}

func (d dep) exists() bool {
	_, err := os.Stat(d.Path)
	return err == nil || !os.IsNotExist(err)
}

func (d dep) mkdir() error {
	return os.MkdirAll(d.Path, 0755)
}

func (d dep) run(command string, args ...string) error {
	//fmt.Fprintln(os.Stderr, d.Path, "->", append([]string{command}, args...))
	fmt.Fprintln(os.Stderr, command, strings.Join(args, " "))
	cmd := exec.Command(command, args...)
	cmd.Dir = d.Path
	return cmd.Run()
}

func (d dep) output(command string, args ...string) (string, error) {
	//fmt.Fprintln(os.Stderr, d.Path, "->", append([]string{command}, args...))
	cmd := exec.Command(command, args...)
	cmd.Dir = d.Path
	b, err := cmd.Output()
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(b)), nil
}

type git struct{}

func (git) restore(d dep) error {
	if !d.exists() {
		if err := (git{}).clone(d); err != nil {
			return err
		}
	} else {
		if (git{}).id(d) == d.ID {
			return nil
		}
		if err := (git{}).fetch(d); err != nil {
			return err
		}
	}
	fmt.Println("restoring", d.Path, "to", d.ID)
	return d.run("git", "checkout", d.ID)
}

func (git) id(d dep) string {
	commit, _ := d.output("git", "rev-parse", "HEAD")
	return commit
}

func (git) clone(d dep) error {
	if err := d.mkdir(); err != nil {
		return err
	}
	return d.run("git", "clone", d.RepoPath, ".")
}

func (git) fetch(d dep) error {
	return d.run("git", "fetch")
}

func (git) update(d dep) error {
	if err := d.run("git", "checkout", "master"); err != nil {
		return err
	}
	return d.run("git", "pull")
}

type hg struct{}

func (hg) restore(d dep) error {
	if !d.exists() {
		if err := (hg{}).clone(d); err != nil {
			return err
		}
	} else {
		if (hg{}).id(d) == d.ID {
			return nil
		}
		if err := (hg{}).pull(d); err != nil {
			return err
		}
	}
	fmt.Println("restoring", d.Path, "to", d.ID)
	return d.run("hg", "update", d.ID)
}

func (hg) id(d dep) string {
	commit, _ := d.output("hg", "id", "-i")
	return commit
}

func (hg) clone(d dep) error {
	if err := d.mkdir(); err != nil {
		return err
	}
	return d.run("hg", "clone", d.RepoPath, ".")
}

func (hg) pull(d dep) error {
	return d.run("hg", "pull")
}

func (hg) update(d dep) error {
	return d.run("hg", "pull", "-u")
}

type bzr struct{}

func (bzr) restore(d dep) error {
	if !d.exists() {
		if err := (bzr{}).clone(d); err != nil {
			return err
		}
	} else {
		if (bzr{}).id(d) == d.ID {
			return nil
		}
		if err := (bzr{}).pull(d); err != nil {
			return err
		}
	}
	fmt.Println("restoring", d.Path, "to", d.ID)
	return d.run("bzr", "update", "-r", d.ID)
}

func (bzr) id(d dep) string {
	commit, _ := d.output("bzr", "version-info", "--custom", "--template", "{revision_id}")
	return commit
}

func (bzr) clone(d dep) error {
	if err := d.mkdir(); err != nil {
		return err
	}
	return d.run("bzr", "branch", d.RepoPath, ".", "--use-existing-dir")
}

func (bzr) pull(d dep) error {
	return d.run("bzr", "pull")
}

func (bzr) update(d dep) error {
	return d.run("bzr", "merge")
}

func collect(ctx *cli.Context) (err error) {
	deps := []dep{}
	exclusions := ctx.StringSlice("X")
	err = filepath.Walk("src", func(path string, info os.FileInfo, err error) error {
		packagePath := strings.TrimPrefix(path, "src/")
		for _, x := range exclusions {
			if strings.HasPrefix(packagePath, x) {
				return filepath.SkipDir
			}
		}
		if !info.IsDir() {
			return nil
		}
		if _, e := os.Stat(path + "/.hg"); e == nil {
			hg := exec.Command("hg", "id", "-i")
			hg.Dir = path
			out, err := hg.Output()
			if err != nil {
				return err
			}
			deps = append(deps, dep{Path: path, RepoType: repoHg, RepoPath: fmt.Sprintf("https://%s", packagePath), ID: strings.TrimSpace(string(out))})
			return filepath.SkipDir
		}
		if _, e := os.Stat(path + "/.git"); e == nil {
			git := exec.Command("git", "rev-parse", "HEAD")
			git.Dir = path
			out, err := git.Output()
			if err != nil {
				return err
			}
			remote := exec.Command("git", "config", "remote.origin.url")
			remote.Dir = path
			remotePath, err := remote.Output()
			if err != nil {
				return err
			}
			deps = append(deps, dep{Path: path, RepoType: repoGit, RepoPath: strings.TrimSpace(string(remotePath)), ID: strings.TrimSpace(string(out))})
			return filepath.SkipDir
		}
		if _, e := os.Stat(path + "/.bzr"); e == nil {
			bzr := exec.Command("bzr", "version-info", "--custom", "--template", "{revision_id}")
			bzr.Dir = path
			out, err := bzr.Output()
			if err != nil {
				return err
			}
			deps = append(deps, dep{Path: path, RepoType: repoBrz, RepoPath: fmt.Sprintf("https://%s", packagePath), ID: strings.TrimSpace(string(out))})
			return filepath.SkipDir
		}
		return nil
	})
	file, err := os.Create(".minideps")
	if err != nil {
		return err
	}
	defer file.Close()
	j, err := json.MarshalIndent(deps, "", "  ")
	if err != nil {
		return err
	}
	writer := bufio.NewWriter(file)
	if _, err := writer.Write(j); err != nil {
		return err
	}
	if err := writer.Flush(); err != nil {
		return err
	}
	return nil
}

func restore(ctx *cli.Context) (err error) {
	file, err := os.Open(".minideps")
	if err != nil {
		return err
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	var deps []dep
	if err := decoder.Decode(&deps); err != nil {
		return err
	}
	for _, d := range deps {
		switch d.RepoType {
		case repoHg:
			if err := (hg{}).restore(d); err != nil {
				return err
			}
		case repoGit:
			if err := (git{}).restore(d); err != nil {
				return err
			}
		case repoBrz:
			if err := (bzr{}).restore(d); err != nil {
				return err
			}
		}
	}
	return
}

func update(ctx *cli.Context) (err error) {
	file, err := os.Open(".minideps")
	if err != nil {
		return err
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	var deps []dep
	if err := decoder.Decode(&deps); err != nil {
		return err
	}
	for _, d := range deps {
		fmt.Fprintln(os.Stderr, "updating", d.Path)
		switch d.RepoType {
		case repoHg:
			if err := (hg{}).update(d); err != nil {
				return err
			}
		case repoGit:
			if err := (git{}).update(d); err != nil {
				return err
			}
		case repoBrz:
			if err := (bzr{}).update(d); err != nil {
				return err
			}
		}
	}
	return
}
