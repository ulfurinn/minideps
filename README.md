# minideps

`minideps` is a minimally invasive **workspace** dependency tracker for Go projects.

`minideps collect` scans `$GOPATH/src` for external repositories and their current commit IDs. Use the `-X` option to exclude paths you don't want managed by `minideps` (e.g. if you already use git submodules for a specific subproject).  
`minideps restore` restores the recorded state.  
`minideps update` fetches the latest version from remotes.

No mangling of `$GOPATH`, no checked in external code.
